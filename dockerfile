FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD bin/FizzBuzz.class bin/app.class
ENTRYPOINT ["java","-cp","bin","FizzBuzz"]
