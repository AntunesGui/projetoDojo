import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FizzBuzzTest {

	//	@Test
	//	void testeBuzzValida5() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(5);
	//		assertEquals("Buzz", retorno);	
	//	}
	//	@Test
	//	void testBuzzValida3() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(3);
	//		assertEquals("Fizz", retorno);	
	//	}
	//
	//	@Test
	//	void testBuzzValida3e5() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(15);
	//		assertEquals("FizzBuzz", retorno);	
	//	}
	//
	//	@Test
	//	void testBuzzValidaDiferente() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(2);
	//		assertEquals("2", retorno);	
	//	}
	//
	//	@Test
	//	void testBuzzValida6() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(6);
	//		assertEquals("Fizz", retorno);	
	//	}
	//
	//	@Test
	//	void testBuzzValida10() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(10);
	//		assertEquals("Buzz", retorno);	
	//	}
	//
	//
	//	@Test
	//	void testBuzzValida30() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(30);
	//		assertEquals("FizzBuzz", retorno);	
	//	}
	//
	//	@Test
	//	void testBuzzValidaDiferente4() {
	//		FizzBuzz fizzBuzz = new FizzBuzz();
	//		String retorno = fizzBuzz.validaNumero(4);
	//		assertEquals("4", retorno);	
	//	}

	@Test
	public void testBuzzImprimeValores() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.imprimeValores(15);
		assertEquals("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz", retorno);	
	}
}

